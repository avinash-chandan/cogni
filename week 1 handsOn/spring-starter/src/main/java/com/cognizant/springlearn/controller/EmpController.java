package com.cognizant.springlearn.controller;

/**
 * @author AVINASH
 *
 */
import java.awt.PageAttributes.MediaType;
import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.springlearn.bean.Employee;
import com.cognizant.springlearn.dao.EmployeeDao;
import com.cognizant.springlearn.service.EmployeeService;

@RestController
public class EmpController {
	@Autowired
	private EmployeeService employeeService;
	@RequestMapping(value="/employee",method = RequestMethod.GET)
	public ArrayList<Employee>  getAllEmp()
	{
		return employeeService.getAllEmployee();

	}
	@PostMapping("/addEmp")
	public void addemp(@RequestBody @Valid  Employee employee)
	{
		System.out.println("The emp to be added"+employee.toString());
		employeeService.addEmp(employee);
	}
	@PutMapping("/updateEmp")
	public void update(@RequestBody @Valid Employee employee)
	{
		employeeService.updateEmp(employee);
	}
	@DeleteMapping("/deleteEmp/{id}")
	public void delete(@PathVariable int id)
	{
		employeeService.delete(id);
	}


}
