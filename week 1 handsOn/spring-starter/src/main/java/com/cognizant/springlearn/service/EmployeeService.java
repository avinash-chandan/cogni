package com.cognizant.springlearn.service;

import java.util.ArrayList;
import java.util.Iterator;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.cognizant.springlearn.SpringStarterApplication;
import com.cognizant.springlearn.bean.Country;
import com.cognizant.springlearn.bean.Employee;
import com.cognizant.springlearn.dao.EmployeeDao;
import com.cognizant.springlearn.exception.EmployeeNotFoundExceptoin;

@Service
@Component
public class EmployeeService {
	@Autowired
	SpringStarterApplication springSAPP;
	public  ArrayList<Employee> employeeList=new ArrayList<>();

	{
		employeeList=springSAPP.getEmployee();
	}
	public ArrayList<Employee> getAllEmployee()
	{
		return employeeList;
	}
	public void addEmp(Employee emp)
	{
		employeeList.add(emp);
	}
	public void updateEmp(Employee empl) {
		// TODO Auto-generated method stub
		boolean flag=true;
		for(int i=0;i<employeeList.size();i++)
		{
			if(employeeList.get(i).getId()==empl.getId())
			{
				flag=false;
				employeeList.set(i,empl);
				break;
			}
		}
		if(flag)
		{
			throw new EmployeeNotFoundExceptoin("This employee is not belong to us");
		}

	}
	public void delete(int id) {
		// TODO Auto-generated method stub
		Iterator<Employee> it=employeeList.iterator();
		boolean flag=true;
		while(it.hasNext())
		{

			Employee em=it.next();
			if(em.getId()==id)
			{
				it.remove();
				flag=false;
				break;
			}
		}
		if(flag)
			throw new EmployeeNotFoundExceptoin("Sorry we can't found this employee!");

	}


	}


