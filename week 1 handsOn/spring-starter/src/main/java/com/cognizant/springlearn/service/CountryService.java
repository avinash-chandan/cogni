package com.cognizant.springlearn.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.cognizant.springlearn.SpringStarterApplication;
import com.cognizant.springlearn.bean.Country;
import com.cognizant.springlearn.bean.CountryList;

@Service
public class CountryService {
	@Autowired
	SpringStarterApplication springSAPP;
	ArrayList<Country> countryList=new ArrayList<>();

	{
		countryList=springSAPP.displayCountries();
	}
	public Country getCountry(String code)
	{
		for(Country country:countryList)
		{
			if(country.getCode().equalsIgnoreCase(code))
				return country;
		}
		return null;
	}
	public ArrayList<Country> getAllCon() {
		// TODO Auto-generated method stub
		return countryList;
	}
	public void setCountry(Country country) {
		// TODO Auto-generated method stub
		countryList.add(country);

	}
}
