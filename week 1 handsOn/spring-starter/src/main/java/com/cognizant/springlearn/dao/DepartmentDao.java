package com.cognizant.springlearn.dao;

import java.util.ArrayList;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.cognizant.springlearn.bean.Department;

@Component
public class DepartmentDao {
	private static ArrayList<Department> departmentList=new ArrayList<>();
	public DepartmentDao(ArrayList<Department> department)
	{
		departmentList=department;
	}
	public DepartmentDao()
	{

	}
	public static ArrayList<Department> getDept() {
		return departmentList;
	}
	public static void setDept(ArrayList<Department> departmentList) {
		DepartmentDao.departmentList = departmentList;
	}
	public ArrayList<Department> getAllDepart()
	{

		ApplicationContext context=new
				ClassPathXmlApplicationContext("employee.xml");
		DepartmentDao d=(DepartmentDao) context.getBean("depart");
		 return d.getDept();


	}


}
