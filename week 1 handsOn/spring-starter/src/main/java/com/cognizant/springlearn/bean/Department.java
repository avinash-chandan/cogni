package com.cognizant.springlearn.bean;


import com.cognizant.springlearn.SpringStarterApplication;
public class Department {


	private String name;
	private String code;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public Department(String name, String code) {
		super();
		this.name = name;
		this.code = code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@Override
	public String toString() {
		return "Department [name=" + name + ", code=" + code + "]";
	}

}
