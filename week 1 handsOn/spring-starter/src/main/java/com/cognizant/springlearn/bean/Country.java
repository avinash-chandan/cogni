package com.cognizant.springlearn.bean;

import javax.validation.constraints.Size;

import org.springframework.lang.NonNull;

import com.cognizant.springlearn.SpringStarterApplication;

public class Country {
	public Country() {

	}
	@NonNull
	@Size(min=2,max=2,message="Country code should be of 2 characters only!")
	private String code;
	private String name;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;

	}

	public Country(String code, String name) {
		super();
		this.code = code;
		this.name = name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Country [code=" + code + ", name=" + name + "]";
	}
}
