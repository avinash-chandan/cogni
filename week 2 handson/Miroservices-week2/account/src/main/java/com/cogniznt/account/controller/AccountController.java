package com.cogniznt.account.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.cogniznt.account.model.Account;

@RestController
public class AccountController {
	static ArrayList<Account> accountList=new ArrayList<>();
	static
	{

		accountList.add(new Account(789456,"saving",85686));
		accountList.add(new Account(7894,"saving",43245));
		accountList.add(new Account(1234,"saving",23234));
	}
	@GetMapping("/accounts/{number}")
	public Account getActByNum(@PathVariable long number)
	{
	       for(Account acc:accountList)
	       {
	    	   if(acc.getNumber()==number)
	    		   return acc;
	       }

	    	return   null;
	}


}
