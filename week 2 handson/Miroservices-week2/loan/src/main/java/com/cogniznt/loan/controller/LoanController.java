package com.cogniznt.loan.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.cogniznt.loan.model.Loan;

@RestController
public class LoanController {
	public static ArrayList<Loan> loanList=new ArrayList<>();
	static
	{
		loanList.add(new Loan(12345,"CAR",400000,8000,18));
		loanList.add(new Loan(123456,"HOME",4000000,8000,12));
		loanList.add(new Loan(1234,"BIKE",40000,800,18));
	}
	@GetMapping("/loans/{number}")
	public Loan getLoanById(@PathVariable long number)
	{
		for(Loan l:loanList)
		{
			if(l.getNumber()==number)
			{
				return l;
			}
		}
		return null;
	}

}
