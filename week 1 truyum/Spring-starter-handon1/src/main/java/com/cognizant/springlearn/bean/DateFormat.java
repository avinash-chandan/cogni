package com.cognizant.springlearn.bean;

public class DateFormat {

	String date;

	@Override
	public String toString() {
		return "DateFormat [date=" + date + "]";
	}

	public DateFormat(String date) {
		super();
		this.date = date;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
}
