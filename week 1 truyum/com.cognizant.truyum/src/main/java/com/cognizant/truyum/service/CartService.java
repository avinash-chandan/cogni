package com.cognizant.truyum.service;


import java.util.ArrayList;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.cognizant.truyum.dao.CartImp;
import com.cognizant.truyum.model.Cart;
import com.cognizant.truyum.model.MenuItem;
@Component
@Service
public class CartService {

	@Autowired
	private MenuItemService ser;
	@Autowired
	private CartImp cService;
	public ArrayList<Cart>getAllCar()
	{
		 return cService.getAllCartItems();
	}
	public void addCartItem(Cart cart) {
		// TODO Auto-generated method stub
		cService.addCart(cart);

	}
	public MenuItem getAllItems(long id, String userId) {
		// TODO Auto-generated method stub
		ArrayList<MenuItem> miList=ser.getAllMenu();
		for(MenuItem mi:miList)
		{
			if(mi.getId()==id)
			{
				return mi;
			}
		}

		return null;

	}
	public void delete(long menuItemId) {
	    //Local variable
		ArrayList<Cart> crt=cService.getAllCartItems();
		Iterator<Cart> it=cService.getAllCartItems().iterator();
		 while(it.hasNext())
		 {
		     //Matching id
			 if(it.next().getProductId()==menuItemId)
			 {
				 it.remove();
			 }
		 }
}
}
