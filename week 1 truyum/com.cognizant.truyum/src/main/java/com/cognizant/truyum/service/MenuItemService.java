package com.cognizant.truyum.service;

import java.util.ArrayList;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.cognizant.truyum.dao.MenuItemDaoCollectionImpl;
import com.cognizant.truyum.model.MenuItem;

@Service
@Component
public class MenuItemService {

	@Autowired
	private MenuItemDaoCollectionImpl menuService;
	public ArrayList<MenuItem> getAllMenu()
	{
		return menuService.getMenuItemListCustomer();
	}
	public MenuItem getItemById(int id) {
		// TODO Auto-generated method stub
		for(MenuItem mi:menuService.getMenuItemListCustomer())
		{
			if(mi.getId()==id)
			{
				return mi;
			}
		}
		return null;
	}
	public void updateMenu(MenuItem menu) {
		// TODO Auto-generated method stub
		boolean flag = true;
		for(int i=0;i<menuService.getMenuItemListCustomer().size();i++)
		{

			MenuItem mt=menuService.getMenuItemListCustomer().get(i);
			if(mt.getId()==menu.getId())
			{
				menuService.getMenuItemListCustomer().set(i,menu);
				flag=false;
				break;
			}
		}
		if(flag)
			menuService.getMenuItemListCustomer().add(menu);



	}
}
